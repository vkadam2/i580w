const {inspect} = require('util'); //for debugging

'use strict';

class DocFinder {

  /** Constructor for instance of DocFinder. */
  
  constructor() {
    this.noiseWordsSet = new Set();
    this.indexes = new Map();
    this.fileNames = new Map();
    this.lineContent = new Map();
    this.allWords = new Set();
    //@TODO
  }

  /** Return array of non-noise normalized words from string content.
   *  Non-noise means it is not a word in the noiseWords which have
   *  been added to this object.  Normalized means that words are
   *  lower-cased, have been stemmed and all non-alphabetic characters
   *  matching regex [^a-z] have been removed.
   */
  words(content) {
    //@TODO
    var wordContentArr = this._wordsLow(content)
	.map((w) => normalize(w.word));
    var temp = wordContentArr.filter((w) => !this.is_noise_word(w));
    return temp;
  }
  normalizeContent(value, index, array){
    return value;
  }

  wordsContent(content) {
    //@TODO
    return this._wordsLow(content)
	.map((w) => [normalize(w.word), w.offset])
	.filter((w) => !this.is_noise_word(w.word));
  }

  is_noise_word(word){
    var flag = this.noiseWordsSet.has(word);
    return flag;
  }

  /** Add all normalized words in noiseWords string to this as
   *  noise words. 
   */
  addNoiseWords(noiseWords) {
    var noiseWordTemp = this.words(noiseWords);
  
    for(var i=0; i<noiseWordTemp.length;i++)
    {
      this.noiseWordsSet.add(noiseWordTemp[i]);
    }
    //@TODO    			      			    
  }

  /** Add document named by string name with specified content to this
   *  instance. Update index in this with all non-noise normalized
   *  words in content string.
   */ 
  addContent(name, content) {
  
    this.fileNames.set(name, content);
    var lines = content.split("\n");
 
    var key = "";
    var value = "";
    var keyName = "";
    var count = 0;
    var offset = 0;
    var sen = "";
    var word = "";
  
    for(var j=0;j<lines.length;j++)
    {
    
      if(lines[j] !=="")
      {
     	var contentsArr = this.wordsContent(lines[j]);
	for(var i=0; i<contentsArr.length;i++)
  	{
    	  count = 0;
    	  value = "";
    	  word = contentsArr[i][0];
    	  key = name + ":" +word;
    	  this.allWords.add(word);
    
    	  offset = j;//contentsArr[i][1];
    
    	  if(!this.lineContent.has(key))
    	  {
	    this.lineContent.set(key, lines[j]);
    	  }
    	  if(this.indexes.has(key))
    	  {
      
      	    var splitArr = this.indexes.get(key).split(":");
      	    count = parseInt(splitArr[0]);
      
      	    offset = splitArr[1];
      
    	  }
      
    	  count = count + 1;
    
    	  value = count + ":" + offset + ":" + sen; 
      	  this.indexes.set(key, value);
	
  	}
      }
    }  
    //@TODO	   
  }

  /** Given a list of normalized, non-noise words search terms, 
   *  return a list of Result's  which specify the matching documents.  
   *  Each Result object contains the following properties:
   *     name:  the name of the document.
   *     score: the total number of occurrences of the search terms in the
   *            document.
   *     lines: A string consisting the lines containing the earliest
   *            occurrence of the search terms within the document.  Note
   *            that if a line contains multiple search terms, then it will
   *            occur only once in lines.
   *  The Result's list must be sorted in non-ascending order by score.
   *  Results which have the same score are sorted by the document name
   *  in lexicographical ascending order.
   *
   */
  find(terms) {
    //@TODO

    var key = "";
    var array = [];
    var tempMap = new Map();
    var searchContent = "";
    var indexTemp = this.indexes;
    var fileNameTemp = this.fileNames;
    var sen = "";

    var offsetLine = [];
    for(var i=0;i<terms.length;i++)
    {
      for(let [file, content] of fileNameTemp)
      {
        key = file + ":" + terms[i].trim();
	
        if(indexTemp.has(key))
	{
	  
	  var value = indexTemp.get(key).split(":");
	  var count = parseInt(value[0]);
	  var offset = parseInt(value[1]);
	  
	  if(tempMap.has(file))
	  {
	    var temp = tempMap.get(file).split(":");
	    count = count + parseInt(temp[0]);  
	  }
	  value = count + ":" + offset + ":" + searchContent + ":" + sen;
	  
	  offsetLine.push({"file":file, "offset":parseInt(offset), "line":this.lineContent.get(key)})
	  tempMap.set(file, value)
	  
	}
      }      

    }

    for(let [key, value] of tempMap)
    {
	var arr = [];
	for(var i=0;i<offsetLine.length;i++)
	{
	  if(offsetLine[i].file === key)
	    arr.push(offsetLine[i]);
	}
	arr.sort(function(a,b){return a.offset - b.offset});
	var sen = "";
        for(var i=0;i<arr.length;i++)
	{
	  if(sen.indexOf(arr[i].line) === -1)
	    sen = sen + arr[i].line + "\n";
	}
        var fileContent = fileNameTemp.get(key);
        var splittedValue = value.split(":");
        var word = splittedValue[2];
      
        let result = new Result(key,parseInt(splittedValue[0]), sen);
	array.push(result);
    }
    array.sort(function(a,b){return compareResults(a,b)});
    return array;
  }

  /** Given a text string, return a ordered list of all completions of
   *  the last word in text.  Returns [] if the last char in text is
   *  not alphabetic.
   */
  complete(text) {
    //@TODO
    if(text === undefined || text === null || text === "")
      return [];
    var words = text.split(" ");
    var wordToSearch = words[words.length-1].toLowerCase(); 
    var arr = [];
    for(let word of this.allWords)
    {
      word = word + "";
      if(word.startsWith(wordToSearch)) 
	arr.push(word);
    }
    return arr;
  }
  
  _wordsLow(content){
    let match;
    let row;
    let arr = [];
    while(match = WORD_REGEX.exec(content)){
      const [word, offset] = [match[0], match.index];
      row = {word,offset};
      arr.push(row);
    
    }
    return arr;
  }
} //class DocFinder

module.exports = DocFinder;

/** Regex used for extracting words as maximal non-space sequences. */
const WORD_REGEX = /\S+/g;
//const SENTENSE_REGEX = ^\n.*;

/** A simple class which packages together the result for a 
 *  document search as documented above in DocFinder.find().
 */ 
class Result {
  constructor(name, score, lines) {
    this.name = name; this.score = score; this.lines = lines;
  }

  toString() { return `${this.name}: ${this.score}\n${this.lines}`; }
}

/** Compare result1 with result2: higher scores compare lower; if
 *  scores are equal, then lexicographically earlier names compare
 *  lower.
 */
function compareResults(result1, result2) {
  return (result2.score - result1.score) ||
    result1.name.localeCompare(result2.name);
}

/** Normalize word by stem'ing it, removing all non-alphabetic
 *  characters and converting to lowercase.
 */
function normalize(word) {
  return stem(word.toLowerCase()).replace(/[^a-z]/g, '');
}

/** Place-holder for stemming a word before normalization; this
 *  implementation merely removes 's suffixes.
 */
function stem(word) {
  return word.replace(/\'s$/, '');
}

