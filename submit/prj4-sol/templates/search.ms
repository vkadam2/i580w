<!DOCTYPE html>
<html>
  <head>
    <title>Documents Collection Search</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
    <h1>Documents Collection Search</h1>
    <ul>
      {{#errors}}
        <li class="error">{{.}}</li>
      {{/errors}}
    </ul>
    <form method="GET" action="{{{base}}}/search.html">
       <p>
	 Please fill in one or more search terms in the field below:
        </p>
	{{#fields}}
	  {{#isSearch}}
	    <label>
	    <span class="label">{{friendlyName}}:</span>
	    <input name="q" {{#type}}type="{{type}}"{{/type}}
	           value="{{value}}">
	    </label>
	    <br/>
	    {{#errorMessage}}
	      <span class="error">{{errorMessage}}</span><br/>
	    {{/errorMessage}}
	  {{/isSearch}}
	 {{/fields}}
      <input name="submit" type="submit" value="search" class="control">
    </form>
{{#fields}}
   {{#searchResult.length}}
   <h2>Search Results</h2>
   {{#searchResult}}
      <p class="result">
	  <a class="doc-name" href="{{{href}}}">{{name}}</a><br>
	  {{{lines}}}
	 <br>
       </p>
   {{/searchResult}}
  {{/searchResult.length}}
{{/fields}}
    {{>footer}}
  </body>
</html>
