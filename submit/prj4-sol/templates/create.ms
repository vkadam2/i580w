<!DOCTYPE html>
<html>
  <head>
    <title>Add Document</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
    <h1>Add Document</h1>
    <form method="POST" action="{{{base}}}/create.html" enctype="multipart/form-data">
	{{#fields}}
	   {{^isSearch}}
	    <label>
	    <span class="label">
	      {{#isRequired}}* {{/isRequired}}{{friendlyName}}:
	    </span>
	    <input name="{{name}}" {{#type}}type="{{type}}"{{/type}}
	           value="{{value}}">
	    </label>
	    <br/>
	    {{#errorMessage}}
	      <span class="error">{{errorMessage}}</span><br/>
	    {{/errorMessage}}
	   {{/isSearch}}
	 {{/fields}}
      <input name="submit" type="submit" value="add" class="control">
    </form> 
    {{>footer}}
  </body>
</html>
