<!DOCTYPE html>
<html>
  <head>
    {{#fields}}<title>{{name}}</title>{{/fields}}
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
    {{#fields}}
    <h1 class="doc-name">{{name}}</h1>
    <form>
	    <pre class="content">{{content}}</pre>
    </form> 
    {{/fields}}
    {{>footer}}
  </body>
</html>
