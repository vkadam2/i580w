'use strict';

const express = require('express');
const upload = require('multer')();
const fs = require('fs');
const mustache = require('mustache');
const Path = require('path');
const { URL } = require('url');
const bodyParser = require('body-parser');
const querystring = require('querystring');

const STATIC_DIR = 'statics';
const TEMPLATES_DIR = 'templates';
const DOCS = '/docs';

function serve(port, base, model) {
  const app = express();
  app.locals.port = port;
  app.locals.base = base;
  app.locals.model = model;
  process.chdir(__dirname);
  app.use(base, express.static(STATIC_DIR));
  setupTemplates(app, TEMPLATES_DIR);
  setupRoutes(app);
  app.listen(port, function() {
    console.log(`listening on port ${port}`);
  });
}


module.exports = serve;

/******************************** Routes *******************************/

function setupRoutes(app) {
  //@TODO add appropriate routes
  const base = app.locals.base;

  app.get(`${base}/search.html`, doSearch(app));
  app.get(`${base}/create.html`, createDocForm(app));
  app.post(`${base}/create.html`, upload.single('file'), createDoc(app));
  app.get(`${base}/:name`, getDoc(app));
}

const FIELDS_INFO = {
  file: {
    friendlyName: 'Choose file',
    type: 'file',
  },
  searchTerms: {
    friendlyName: 'Search Terms',
    isSearch: 'true',
    error: 'Please specify one-or-more search terms',
  },
};

const FIELDS =
  Object.keys(FIELDS_INFO).map((n) => Object.assign({name: n}, FIELDS_INFO[n]));

/*************************** Action Routines ***************************/

//@TODO add action routines for routes + any auxiliary functions.

function createDocForm(app) {
  return async function(req, res) {
    const model = { base: app.locals.base, fields: FIELDS };
    const html = doMustache(app, 'create', model);
    res.send(html);
  };
};

function doSearch(app) {
  return async function(req, res) {
    const isSubmit = req.query.submit !== undefined;
    let result = [];
    let errors = undefined;
    const search = getNonEmptyValues(req.query);
    console.log("search : " + JSON.stringify(search));
    if (isSubmit) {
      if (Object.keys(search).length == 0) {
	const msg = 'at least one search parameter must be specified';
	errors = Object.assign(errors || {}, { _: msg });
      }
      if (!errors) {
	const q = querystring.stringify(search);
	try {
	  let response = await app.locals.model.list(q);
          result = response['results'];
          console.log("Result of search = " + JSON.stringify(result));
	}
	catch (err) {
          console.error(err);
	  errors = wsErrors(err);
	}

	if (result.length === 0) {
	  errors = {_: 'no users found for specified criteria; please retry'};
	}
      }
    }
    let model, template;
    template = 'search';
    if (result.length > 0) {  
       const test2 = {searchResult : result};
       var newArray =      FIELDS.concat(test2);
       model = { base: app.locals.base, fields: newArray };
    }
    else {
      model = errorModel(app, search, errors);
    }
    const html = doMustache(app, template, model);
    res.send(html);
  };
};

function createDoc(app) {
  return async function(req, res) {
	let docFile = req.file;
        const doc = {};
	doc['name'] = getFilenameWithoutExtension(docFile.originalname);
	doc['content'] = docFile.buffer.toString('utf8');
	try {
	  await app.locals.model.create(doc);
	  res.redirect(`${app.locals.base}/${doc['name']}`);
	}
	catch (err) {
	  console.error(err);
	  errors = wsErrors(err);
        }
  };
};

function getDoc(app) {
  return async function(req, res) {
    let model;
    const name = req.params.name;
    try {
      const doc = await app.locals.model.get(name);
      const FIELDS = [{name: name, content: doc['content']}];
      model = { base: app.locals.base, fields: FIELDS };
    }
    catch (err) {
      console.error(err);
      const errors = wsErrors(err);
      model = errorModel(app, {}, errors);
    }
    const html = doMustache(app, 'details', model);
    res.send(html);
  };
};

function getFilenameWithoutExtension(filename){
    var lastDotPosition = filename.lastIndexOf(".");
    if (lastDotPosition === -1) return filename;
    else return filename.substr(0, lastDotPosition);
}

/************************** Field Utilities ****************************/

/** Return copy of FIELDS with values and errors injected into it. */
function fieldsWithValues(values, errors={}) {
  return FIELDS.map(function (info) {
    const name = info.name;
    const extraInfo = { value: values[name] };
    if (errors[name]) extraInfo.errorMessage = errors[name];
    return Object.assign(extraInfo, info);
  });
}

/** Return a model suitable for mixing into a template */
function errorModel(app, values={}, errors={}) {
  return {
    base: app.locals.base,
    errors: errors._,
    fields: fieldsWithValues(values, errors)
  };
}
/************************ General Utilities ****************************/

/** return object containing all non-empty values from object values */
function getNonEmptyValues(values) {
  const out = {};
  Object.keys(values).forEach(function(k) {
    const v = values[k];
    if (v && v.trim().length > 0) out[k] = v.trim();
  });
  return out;
}


/** Return a URL relative to req.originalUrl.  Returned URL path
 *  determined by path (which is absolute if starting with /). For
 *  example, specifying path as ../search.html will return a URL which
 *  is a sibling of the current document.  Object queryParams are
 *  encoded into the result's query-string and hash is set up as a
 *  fragment identifier for the result.
 */
function relativeUrl(req, path='', queryParams={}, hash='') {
  const url = new URL('http://dummy.com');
  url.protocol = req.protocol;
  url.hostname = req.hostname;
  url.port = req.socket.address().port;
  url.pathname = req.originalUrl.replace(/(\?.*)?$/, '');
  if (path.startsWith('/')) {
    url.pathname = path;
  }
  else if (path) {
    url.pathname += `/${path}`;
  }
  url.search = '';
  Object.entries(queryParams).forEach(([k, v]) => {
    url.searchParams.set(k, v);
  });
  url.hash = hash;
  return url.toString();
}

/************************** Template Utilities *************************/


/** Return result of mixing view-model view into template templateId
 *  in app templates.
 */
function doMustache(app, templateId, view) {
  const templates = { footer: app.templates.footer };
  return mustache.render(app.templates[templateId], view, templates);
}

/** Add contents all dir/*.ms files to app templates with each 
 *  template being keyed by the basename (sans extensions) of
 *  its file basename.
 */
function setupTemplates(app, dir) {
  app.templates = {};
  for (let fname of fs.readdirSync(dir)) {
    const m = fname.match(/^([\w\-]+)\.ms$/);
    if (!m) continue;
    try {
      app.templates[m[1]] =
	String(fs.readFileSync(`${TEMPLATES_DIR}/${fname}`));
    }
    catch (e) {
      console.error(`cannot read ${fname}: ${e}`);
      process.exit(1);
    }
  }
}