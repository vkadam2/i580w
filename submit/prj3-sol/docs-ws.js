'use strict';

const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const process = require('process');
const url = require('url');
const queryString = require('querystring');

const OK = 200;
const CREATED = 201;
const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const CONFLICT = 409;
const SERVER_ERROR = 500;


//Main URLs
const DOCS = '/docs';
const COMPLETIONS = '/completions';

//Default value for count parameter
const COUNT = 5;
const START = 0;

/** Listen on port for incoming requests.  Use docFinder instance
 *  of DocFinder to access document collection methods.
 */
function serve(port, docFinder) {
  const app = express();
  app.locals.port = port;
  app.locals.finder = docFinder;
  setupRoutes(app);
  const server = app.listen(port, async function() {
    console.log(`PID ${process.pid} listening on port ${port}`);
  });
  return server;
}

module.exports = { serve };

function setupRoutes(app) {
  app.use(cors());            //for security workaround in future projects
  app.use(bodyParser.json()); //all incoming bodies are JSON
  //@TODO: add routes for required 4 services
  app.get(`${DOCS}/:name`, doGetContent(app));
  app.get(DOCS, doSearchContent(app));
  app.post(DOCS, doAddContent(app));
  app.get(COMPLETIONS, doGetCompletions(app));
  app.use(doErrors()); //must be last; setup for server errors   
}

//@TODO: add handler creation functions called by route setup
//routine for each individual web service.  Note that each
//returned handler should be wrapped using errorWrap() to
//ensure that any internal errors are handled reasonably.

function doGetContent(app){
  return errorWrap(async function(req, res){    
    try{
      const {name}  = req.params;
      const tempResult =  await app.locals.finder.docContent(name);
      var Result = {"content":tempResult,
		    "links" : [{
			        "rel" : "self",
			        "href" : baseUrl(req, DOCS + '/' + name)
			       }]};
      res.json(Result);
    }
    catch(err){
      const mapped = mapError(err);
      res.status(mapped.status).json(mapped);
    }
  });
}

function doGetCompletions(app){
  return errorWrap(async function(req, res){    
    try{
      	const queryParams  = req.query || {}; 
	let text = queryParams.text;
	if(text === undefined) {
	   res.status(BAD_REQUEST);
    	   res.json({ code: 'BAD_PARAM', message: "required query parameter \"text\" is missing"});
	}
	else {
      	  const tempResult =  await app.locals.finder.complete(text);
      	  res.json(tempResult);
	}
    }
    catch(err){
      const mapped = mapError(err);
      res.status(mapped.status).json(mapped);
    }
  });
}

function doAddContent(app){
  return errorWrap(async function(req, res){    
    try{
      	const obj = req.body;
	if(obj.name === undefined) {
	   res.status(BAD_REQUEST);
    	   res.json({ code: 'BAD_PARAM', message: "required body parameter \"name\" is missing"});
	}
	else if(obj.content === undefined) {
	   res.status(BAD_REQUEST);
    	   res.json({ code: 'BAD_PARAM', message: "required body parameter \"content\" is missing"});
	}
	else {
	   await app.locals.finder.addContent(obj.name, obj.content); 
  	   let url = baseUrl(req, DOCS + '/' + obj.name);
	   var Result = {"href":url};
	   res.location(url);
	   res.status(CREATED).json(Result);
	}
    }
    catch(err){
      const mapped = mapError(err);
      res.status(mapped.status).json(mapped);
    }
  });
}

function doSearchContent(app){
  return errorWrap(async function(req, res){      
    try{      
      const queryParams  = req.query || {}; 
      let q = queryParams.q;
      let start = queryParams.start;
      let count = queryParams.count;
      if(q === undefined) {
    	res.status(BAD_REQUEST);
    	res.json({ code: 'BAD_PARAM', message: "required query parameter \"q\" is missing"});
      }
      else {
      if(start === undefined) {
	start = parseInt(START);
      }
      else {
	start = parseInt(start);
      }

      if(count === undefined) {
	count = parseInt(COUNT);
      }
      else {
	count = parseInt(count);
      }

      if(isNaN(start) || start < 0) {
	res.status(BAD_REQUEST);
	res.json({ code: 'BAD_PARAM', message: "bad query parameter \"start\""});
      }
      else if(isNaN(count) || count < 0) {
	res.status(BAD_REQUEST);
	res.json({ code: 'BAD_PARAM', message: "bad query parameter \"count\""});
      }
      else {	
	const searchResults =  await app.locals.finder.find(q); 
	const totalCount = searchResults.length;
	const results = [];  
	for(let i = start; i < totalCount; i++) {
		if(i >= (start + count))
		   break;
		searchResults[i].href = baseUrl(req, DOCS + '/' + searchResults[i].name);
		results.push(searchResults[i]);
	}
	q = q.replace(' ','%20');
	var links = [{
		       "rel" : "self",
		       "href" : baseUrl(req, DOCS + '?q=' + q  + '&start=' + start + '&count=' + count)
		     }];

	if(start > 0){
	links.push({
			"rel" : "previous",
		        "href" : baseUrl(req, DOCS + '?q=' + q  + '&start=' + ((start - count) < 0 ? 0 : (start - count)) + '&count=' + count)
		   });
	};
	if((start + count) < totalCount) {
	links.push({
			"rel" : "next",
		        "href" : baseUrl(req, DOCS + '?q=' + q  + '&start=' + (start + count) + '&count=' + count)
		   });
	};

	var Result = {"results":results,
		    "totalCount" : totalCount,
		    "links" : links};
	res.json(Result);
	}
      }
    }
    catch(err){
      const mapped = mapError(err);
      res.status(mapped.status).json(mapped);
    }
  });
}

/** Return error handler which ensures a server error results in nice
 *  JSON sent back to client with details logged on console.
 */ 
function doErrors(app) {
  return async function(err, req, res, next) {
    res.status(SERVER_ERROR);
    res.json({ code: 'SERVER_ERROR', message: err.message });
    console.error(err);
  };
}

/** Set up error handling for handler by wrapping it in a 
 *  try-catch with chaining to error handler on error.
 */
function errorWrap(handler) {
  return async (req, res, next) => {
    try {
      await handler(req, res, next);
    }
    catch (err) {
      next(err);
    }
  };
}

/*************************** Mapping Errors ****************************/

const ERROR_MAP = {
  EXISTS: CONFLICT,
  NOT_FOUND: NOT_FOUND
}

/** Map domain/internal errors into suitable HTTP errors.  Return'd
 *  object will have a "status" property corresponding to HTTP status
 *  code.
 */
function mapError(err) {
  return { status: (ERROR_MAP[err.code] || SERVER_ERROR),
	code: (err.code || 'INTERNAL'),
	message: err.message
      };
}
  

/** Return base URL of req for path.
 *  Useful for building links; Example call: baseUrl(req, DOCS)
 */
function baseUrl(req, path='/') {
  const port = req.app.locals.port;
  const url = `${req.protocol}://${req.hostname}:${port}${path}`;
  return url;
}
