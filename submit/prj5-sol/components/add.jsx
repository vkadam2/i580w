//-*- mode: rjsx-mode;

'use strict';

const React = require('react');

class Add extends React.Component {

  /** called with properties:
   *  app: An instance of the overall app.  Note that props.app.ws
   *       will return an instance of the web services wrapper and
   *       props.app.setContentName(name) will set name of document
   *       in content tab to name and switch to content tab.
   */
  constructor(props) {
    super(props);
    //@TODO
	this.ws = props.app.ws;
	this.state = { selectedFile: null};
	this.handleChange = this.handleChange.bind(this);
	this.onFormSubmit = this.onFormSubmit.bind(this);
	
  }

  //@TODO add code

  //Note that a you can get information on the file being uploaded by
  //hooking the change event on <input type="file">.  It will have
  //event.target.files[0] set to an object containing information
  //corresponding to the uploaded file.  You can get the contents
  //of the file by calling the provided readFile() function passing
  //this object as the argument.


  render() {
    //@TODO
    return (
      <form onSubmit={this.onFormSubmit}>
      <label className="label">
	Choose File: 
        <input type="file" className = "control" onChange={this.handleChange} />
      </label>
	<div className ='error'></div>
      </form>
    );
  }

  async handleChange(event) {
    this.setState({
      selectedFile: event.target.files[0]
    });
	const file = event.target.files[0];
	const content = await readFile(file);
	//console.log(file.name);
	//console.log(content);
	//console.log(prom);
	const fileName = file.name.replace('.txt', '');
	await this.ws.addContent(fileName, content);
	
	//console.log(fileName);
	this.props.app.setContentName(fileName);
  }

  onFormSubmit(e){
console.log('inside on submit');
    e.preventDefault(); // Stop form submit
    //alert(this.state.selectedFile);
    /*this.fileUpload(this.state.file).then((response)=>{
      console.log(response.data)
    })*/
  }
  
}

module.exports = Add;

/** Return contents of file (of type File) read from user's computer.
 *  The file argument is a file object corresponding to a <input
 *  type="file"/>
 */
async function readFile(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () =>  resolve(reader.result);
    reader.readAsText(file);
  });
}
