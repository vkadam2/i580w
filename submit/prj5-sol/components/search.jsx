//-*- mode: rjsx-mode;

'use strict';

const React = require('react');

class Search extends React.Component {

  /** called with properties:
   *  app: An instance of the overall app.  Note that props.app.ws
   *       will return an instance of the web services wrapper and
   *       props.app.setContentName(name) will set name of document
   *       in content tab to name and switch to content tab.
   */
  constructor(props) {
    super(props);
    //@TODO
	//this.app = props.app;
	this.ws = props.app.ws;
	this.state = {
            name: '',
	    results: [],
	    length: 0,
	    searchTerm: '',
	    error: ''
        };
	this.onClick = this.onClick.bind(this);
  }

  //@TODO
  handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

  async onSubmit(e) {
       e.preventDefault();
	const word = this.state.name;
	//console.log(word);
        //if(word.trim()=='')
	const output = await this.ws.searchDocs(word, 0);
	//console.log(output);
	let err = '';
	if(output.totalCount == 0)
	{
	   if(word.trim()!==''){
	   err = 'No results for '+ word;
	   }
	}
	this.setState({
            results: output.results,
	    length: output.totalCount,
	    searchTerm: word,
	    error: err
	    
        })
//console.log(this.state.results);
    }

    onClick(e) {
       e.preventDefault();
	//console.log('search a tag on click')
	//console.log(e.currentTarget.innerHTML);
	this.props.app.setContentName(e.currentTarget.innerHTML);
	//const output = await this.ws.searchDocs(word, 0);
	
    }

    renderSearchResult() {
    //let class1 = result;
    //console.log(this.state.results);
    const searchTermArr = this.state.searchTerm.toUpperCase().split(' ')
    let content = [];
    this.state.results.forEach(item=>{
       //console.log(item);
	let lineContent=[];
       item.lines.forEach(line=>{
            const as = line.split(' ');
	    as.forEach(word=>{
		word = word.trim()
		let regexResult = word.match( /[!@#\$%\^\&*\)\(\:\,\"\'\"+=._-]+/i )
		let specialCharRegex = ''
		if(regexResult !== null)
		{
		  specialCharRegex = regexResult[0]
		  //console.log(regexResult[0]);
		}
		
		let isComma = false
		let isSpecialChar = false
		let isSpecialChar1 = false
		word = word.trim()
		//if(word.includes(',')){isComma = true}
		if(word.includes("'s")){isSpecialChar = true}
		if(word.includes(',"')){isSpecialChar1 = true}
		word = word.replace("'s","").replace(',"','').replace(specialCharRegex,'');
		//let comma = '';
		let specialChar = '';
		let specialChar1 = '';
		//if(isComma){comma = ','}
		if(isSpecialChar){specialChar = "'s"
		specialCharRegex=''
		}
		if(isSpecialChar1){specialChar1 = ',"'
		specialCharRegex=''
		}
		if(searchTermArr.includes(word.trim().toUpperCase())){
			lineContent.push(<span className='search-term'>{word}</span>)
		}
		else{
	          lineContent.push(word)
		}
		lineContent.push(specialChar + specialChar1 + specialCharRegex + ' ')
            })
            lineContent.push(<br/>)
          })
       //console.log(lineContent)
       content.push(<div className='result'>
          <u><a className='result-name' onClick={this.onClick} href={item.name}>{item.name}</a></u><br/>
          <p>{lineContent}</p>
	  </div>)
       /*content.push(<h1>{item.name}</h1>)
       content.push(<p>);
       content.push({item.lines})
       content.push(</p>);
       content.push(</div>)*/
    })
    /*for(let i=0;i<this.state.length;i++)
    {
       content.push(<h1>{this.state.results[i].name}</h1>);
    }*/
    return (content)
}
  
  handleSearchSubmit(event) {
    event.preventDefault();
    //console.log('inside handle search submit');
    //console.log(this.textInput.value);
  }

  render() {
    //@TODO
    return (
	<div>
	<form onSubmit={(e) => this.onSubmit(e)}>
	  <label>
	  <span className="label">Search Terms:</span>
	  <span className="control">
	    <input type = "text"
                        name='name'
                        value={this.state.name}
                        onChange={e => this.handleChange(e)}
			onBlur={(e) => this.onSubmit(e)} /><br/>
	  </span>
	  </label>
	</form>
	<div>{this.renderSearchResult()}</div>
	<span className="error">{this.state.error}</span>
	</div>
    );
  }

}

module.exports = Search;
