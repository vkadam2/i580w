const assert = require('assert');
const mongo = require('mongodb').MongoClient;

const {inspect} = require('util'); //for debugging

'use strict';

/** This class is expected to persist its state.  Hence when the
 *  class is created with a specific database url, it is expected
 *  to retain the state it had when it was last used with that URL.
 */ 
class DocFinder {

  /** Constructor for instance of DocFinder. The dbUrl is
   *  expected to be of the form mongodb://SERVER:PORT/DB
   *  where SERVER/PORT specifies the server and port on
   *  which the mongo database server is running and DB is
   *  name of the database within that database server which
   *  hosts the persistent content provided by this class.
   */
  //let mongoUrl = "";
  constructor(dbUrl) {
    //TODO
    //console.log("Hello \n")
    	this.mongoUrl = dbUrl;
	this.noiseWordSetCollection = "noiseWordSet";
	this.indexesCollection = "indexes";
	this.fileContentCollection = "fileContent";
    //console.log(dbUrl);
    //console.log(dbUrl.substring(dbUrl.lastIndexOf("/")+1, dbUrl.length));
  }

  /** This routine is used for all asynchronous initialization
   *  for instance of DocFinder.  It must be called by a client
   *  immediately after creating a new instance of this.
   */
  async init() {
    //TODO
    
    let dbName = this.mongoUrl.substring(this.mongoUrl.lastIndexOf("/")+1, this.mongoUrl.length);
    //let db = "docs";
    this.client = await mongo.connect(this.mongoUrl, MONGO_OPTIONS);
    this.db = this.client.db(dbName);
  }

  /** Release all resources held by this doc-finder.  Specifically,
   *  close any database connections.
   */
  async close() {
    //TODO
    this.client.close();
  }

  /** Clear database */
  async clear() {
    //TODO
	let ret = await this.db.collection(this.noiseWordSetCollection).find().count();
	if(ret > 0)
		await this.db.collection(this.noiseWordSetCollection).drop();

	ret = await this.db.collection(this.fileContentCollection).find().count();
	if(ret > 0)
		await this.db.collection(this.fileContentCollection).drop();

	ret = await this.db.collection(this.indexesCollection).find().count();
	if(ret > 0)
	await this.db.collection(this.indexesCollection).drop();
  }

  /** Return an array of non-noise normalized words from string
   *  contentText.  Non-noise means it is not a word in the noiseWords
   *  which have been added to this object.  Normalized means that
   *  words are lower-cased, have been stemmed and all non-alphabetic
   *  characters matching regex [^a-z] have been removed.
   */
  async words(contentText) {
    //TODO
    const ret = await this.db.collection(this.noiseWordSetCollection).find();
    const result = await ret.toArray();
    return this._wordsLow(contentText)
	.map((w) => [normalize(w.word), w.offset])
	.filter((w) => !this.is_noise_word(result, w));
    
  }

  /*function checkWords(words, nWord) {
    return words.word === nWord;
  }*/

  is_noise_word(noiseWords, nWord){
	
	const t = noiseWords.find(w => w.word === nWord[0]);
//console.log(nWord[0] + t);
	return t == undefined ? false : true;
  }

  _wordsLow(content){
    let match;
    let row;
    let arr = [];
    while(match = WORD_REGEX.exec(content)){
      const [word, offset] = [match[0], match.index];
      row = {word,offset};
      arr.push(row);
    
    }
    return arr;
  }

  /** Add all normalized words in the noiseText string to this as
   *  noise words.  This operation should be idempotent.
   */
  async addNoiseWords(noiseText) {
    //TODO
	
    	var noiseWordTemp = await this.words(noiseText);
	//console.log(noiseWordTemp);
	//console.log(noiseWordTemp);
    	if(noiseWordTemp.length !== 0)
	{
    		let obj = [];
		let wordN = "";
    		for(var i=0; i<noiseWordTemp.length;i++)
    		{
			wordN = noiseWordTemp[i][0];
			if(wordN !== "")
				obj.push({_id: i+1, word: wordN});
    		}
		if(obj.length > 0){
    			const noiseWords = this.db.collection(this.noiseWordSetCollection);
			const ret = await noiseWords.insertMany(obj);
		}
  	}
  }

  /** Add document named by string name with specified content string
   *  contentText to this instance. Update index in this with all
   *  non-noise normalized words in contentText string.
   *  This operation should be idempotent.
   */ 
  async addContent(name, contentText) {
    //TODO
	var query = { _id: name };
	const collection = this.db.collection(this.fileContentCollection)
	let ret = await collection.updateOne({_id: name}, { $set: { content: contentText}}, {upsert:true});
	//let ret = await this.db.collection("fileContent").findOne(query);
	//const ret1 = await this.db.collection("fileContent").find(query, {$exists: true});
	//console.log(ret1);
	/*if(ret == undefined){
		const noiseWords = this.db.collection("fileContent");
	     	const ret = await noiseWords.insertOne({_id: name, content:contentText});
	}*/
	
	query = { fileName:name };
	ret = await this.db.collection(this.indexesCollection).deleteMany(query);
	//console.log(ret.deletedCount);
	var wordMap = new Map();
	var allWords = await this.words(contentText);
	var key = "";
	var offset = 0, count =0;
	var value = "";
	for(let i=0;i<allWords.length;i++)
	{
		key = allWords[i][0];
		offset = allWords[i][1];
		count = 0;
		value = "";
		//if(allWords[i] !=="")
		{
			if(wordMap.has(key))
    	  		{
      				//count = parseInt(wordMap.get(key));
				var splitArr = wordMap.get(key).split(":");
      	    			count = parseInt(splitArr[0]);
	      	    		offset = splitArr[1];
    	  		}
			count = count + 1;
			value = count + ":" + offset;
			wordMap.set(key, value);
		}
	}
	//console.log(wordMap);
	var obj = [];
	for(let [w, value] of wordMap)
	{
		var splitArr = value.split(":");
      	    	let c = parseInt(splitArr[0]);
	      	let o = splitArr[1];
		let key = name+ ":" +w;
		obj.push({_id:key, fileName:name, word:w, count:c, offset:o});
	}
    	const indexesStr = this.db.collection(this.indexesCollection);
	ret = await indexesStr.insertMany(obj);
  }

  /** Return contents of document name.  If not found, throw an Error
   *  object with property code set to 'NOT_FOUND' and property
   *  message set to `doc ${name} not found`.
   */
  async docContent(name) {
    //TODO
	var query = { _id: name };
	let ret = await this.db.collection(this.fileContentCollection).findOne(query);
	if(ret == undefined){
		var myError = new Error();
		myError.code = "NOT_FOUND";
		myError.message = "doc "+ name +" not found";
		throw myError;
	}
	else{
		return ret.content;
	}
  }
  
  /** Given a list of normalized, non-noise words search terms, 
   *  return a list of Result's  which specify the matching documents.  
   *  Each Result object contains the following properties:
   *
   *     name:  the name of the document.
   *     score: the total number of occurrences of the search terms in the
   *            document.
   *     lines: A string consisting the lines containing the earliest
   *            occurrence of the search terms within the document.  The 
   *            lines must have the same relative order as in the source
   *            document.  Note that if a line contains multiple search 
   *            terms, then it will occur only once in lines.
   *
   *  The returned Result list must be sorted in non-ascending order
   *  by score.  Results which have the same score are sorted by the
   *  document name in lexicographical ascending order.
   *
   */
  async find(terms) {
    //TODO
	let key = "";
	const ret = await this.db.collection(this.fileContentCollection).find();
    	const result = await ret.toArray();
	var finalResult = [];
	var tempMap = new Map();
	for(var i=0;i<terms.length;i++)
    	{
      		for(var j=0;j<result.length;j++)
      		{
			//console.log(result[j]._id);
			var fileName = result[j]._id;
			key = fileName + ":" + terms[i][0];
			//console.log(key);
			var query = { _id: key };
			const ret1 = await this.db.collection(this.indexesCollection).findOne(query);
			if(ret1 !== null && ret1 !== undefined)
			{
			//console.log(ret1);
			var offset = parseInt(ret1.offset);
			var count = parseInt(ret1.count);
			var value = "";
			if(tempMap.has(fileName))
	  		{
	    			var temp = tempMap.get(fileName).split(":");
	    			count = count + parseInt(temp[0]);  
	  		}
			value = count + ":" + offset
			let line = this.extractLine(result[j].content, parseInt(ret1.offset));
			finalResult.push({"file":result[j]._id, "offset":offset, "word":ret1.word, "count":count, "line": line});
			tempMap.set(fileName, value)
			}
		}
	}
	//console.log(finalResult);
	/*var array = [];
	for(var i=0;i<finalResult.length;i++)
	{
		let result = new Result(finalResult[i].file, finalResult[i].count, finalResult[i].line);
		array.push(result);
	}
    return array;*/
    var array = [];
    for(let [key, value] of tempMap)
    {
	var arr = [];
	for(var i=0;i<finalResult.length;i++)
	{
	  if(finalResult[i].file === key)
	    arr.push(finalResult[i]);
	}
	arr.sort(function(a,b){return a.offset - b.offset});
	var sen = "";
        for(var i=0;i<arr.length;i++)
	{
	  if(sen.indexOf(arr[i].line) === -1)
	    sen = sen + arr[i].line + "\n";
	}
	//sen = sen + "\n";
        //var fileContent = fileNameTemp.get(key);
        var splittedValue = value.split(":");
        //var word = splittedValue[2];
      
        let result = new Result(key,parseInt(splittedValue[0]), sen);
	array.push(result);
    }
    array.sort(function(a,b){return compareResults(a,b)});
    return array;
  }

  extractLine(text, offset){
  	return text.substring(
            text.slice(0, offset).lastIndexOf('\n')+1, 
	text.substring(offset, text.length).indexOf('\n') === -1 ? text.length : text.substring(offset, text.length).indexOf('\n')+offset);
    }


  /** Given a text string, return a ordered list of all completions of
   *  the last normalized word in text.  Returns [] if the last char
   *  in text is not alphabetic.
   */
  async complete(text) {
    //TODO
    	if(text === undefined || text === null || text === "")
      		return [];
    	var words = text.split(" ");
    	var wordToSearch = words[words.length-1].toLowerCase(); 
	var reg = new RegExp("^" + wordToSearch +".*");
	//console.log(reg);
	let ret = await this.db.collection(this.indexesCollection).find({word: {$regex: reg, $options: "i"}});
	const result = await ret.toArray();
	var newv = result.map(m => m.word);
	var set = new Set(newv);
	//console.log(Array.from(set));
    return Array.from(set);
  }

  //Add private methods as necessary

} //class DocFinder

module.exports = DocFinder;

//Add module global functions, constants classes as necessary
//(inaccessible to the rest of the program).

//Used to prevent warning messages from mongodb.
const MONGO_OPTIONS = {
  useNewUrlParser: true
};

/** Regex used for extracting words as maximal non-space sequences. */
const WORD_REGEX = /\S+/g;

/** A simple utility class which packages together the result for a
 *  document search as documented above in DocFinder.find().
 */ 
class Result {
  constructor(name, score, lines) {
    this.name = name; this.score = score; this.lines = lines;
  }

  toString() { return `${this.name}: ${this.score}\n${this.lines}`; }
}

/** Compare result1 with result2: higher scores compare lower; if
 *  scores are equal, then lexicographically earlier names compare
 *  lower.
 */
function compareResults(result1, result2) {
  return (result2.score - result1.score) ||
    result1.name.localeCompare(result2.name);
}

/** Normalize word by stem'ing it, removing all non-alphabetic
 *  characters and converting to lowercase.
 */
function normalize(word) {
  return stem(word.toLowerCase()).replace(/[^a-z]/g, '');
}

/** Place-holder for stemming a word before normalization; this
 *  implementation merely removes 's suffixes.
 */
function stem(word) {
  return word.replace(/\'s$/, '');
}



